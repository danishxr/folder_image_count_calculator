# Folder_Image_count_calculator

The python code will calculate the number of images inside the sub-folders and give a standardised output in a csv format.


# Usage

`python3 script.py -dir /home/main_folder/`

---------------------------

## Directory Structure

----------------------------

![Imgur](https://i.imgur.com/YcAaKWa.png)

## Final output


| rose    | 3 |
|---------|---|
| jasmine | 3 |
| lotus   | 3 |


